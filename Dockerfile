FROM openjdk:8

ENV DOCKER_VERSION 18.09.6

RUN \
  wget -O docker.tgz https://download.docker.com/linux/static/stable/x86_64/docker-$DOCKER_VERSION.tgz; \
  tar --extract \
  --file docker.tgz \
  --strip-components 1 \
  --directory /usr/local/bin



ENV SBT_VERSION 1.2.7

# Install SBT
RUN \
  wget -Osbt-$SBT_VERSION.deb https://dl.bintray.com/sbt/debian/sbt-$SBT_VERSION.deb && \
  dpkg -i sbt-$SBT_VERSION.deb && \
  rm sbt-$SBT_VERSION.deb && \
  sbt sbtVersion

# Node js for play apps
RUN curl -sL https://deb.nodesource.com/setup_11.x | bash -
RUN apt-get install -y nodejs && apt-get clean